(require 'ox-publish)

(setq org-html-postamble-format
   '(("en" "<p class=\"author\">Author: %a (%e)</p>
<p class=\"date\">Date: %d</p>
<p class=\"creator\">%c</p>
<p class=\"validation\">%v</p>")
     ("es" "<p class=\"author\">Autor: %a (%e)</p>
<p class=\"date\">Modificado: %d</p>
<p class=\"creator\">%c</p>
<p class=\"validation\">%v</p>")))

(setq org-publish-project-alist
      '(
        ("fuentes-org"
         :base-directory "~/proyectos/kvintero/org/"
         :base-extension "org"
         :publishing-directory "~/proyectos/kvintero/html/"
         :language "es"
         :recursive t
         :publishing-function org-html-publish-to-html
         :headline-levels 4
         :html-preamble t
         :html-postamble t)
        ("org-auxiliares"
         :base-directory "~/proyectos/kvintero/org/"
         :base-extension "css\\|js\\|jpg\\|png\\|ttf"
         :publishing-directory "~/proyectos/kvintero/html/"
         :recursive t
         :publishing-function org-publish-attachment)
        ("kvintero" :components ("fuentes-org" "org-auxiliares"))))
